<apex:page docType="html-5.0" controller="dupcheck.dc3ControllerEntry" sidebar="false" title="Entry - Duplicate Check for Salesforce" action="{!janitor}" tabstyle="dcEntry__tab">
    
    <apex:stylesheet value="{!URLFOR($Resource.dupcheck__dc3Assets,'/dc3/assets/css/bootstrap-namespaced.min.css')}" />
    <apex:stylesheet value="{!URLFOR($Resource.dupcheck__dc3Assets,'/dc3/assets/css/fonts.css')}" />
    <apex:stylesheet value="{!AssetsUrlStatic}/css/bootstrap-select.css" />
    <apex:stylesheet value="{!AssetsUrlStatic}/css/datetimepicker.css" />
    <apex:stylesheet value="{!AssetsUrl}/sf1/assets/css/duplicatecheck.css" />
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]--> 

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <apex:includeScript value="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" />
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <apex:includeScript value="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js" />
    <apex:includeScript value="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" />
    <apex:includeScript value="//ajax.googleapis.com/ajax/libs/angularjs/1.4.3/angular.min.js" />
    <apex:includeScript value="//ajax.googleapis.com/ajax/libs/angularjs/1.4.3/angular-sanitize.min.js" />
    <apex:includeScript value="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.13.4/ui-bootstrap-tpls.js" />
    <apex:includeScript value="{!AssetsUrlStatic}/javascript/bootstrap-select.js" />
    <apex:includeScript value="{!AssetsUrlStatic}/javascript/datetimepicker.js" />
    
    <apex:includeScript value="{!AssetsUrlStatic}/javascript/bootbox.min.js" />
    <apex:includeScript value="{!AssetsUrl}/sf1/assets/javascript/duplicatecheck.js" /> 
    <apex:includeScript value="{!AssetsUrl}/sf1/assets/javascript/duplicatecheck.entry.js" />
    
    <apex:includeScript value="/soap/ajax/35.0/connection.js"/>
    <script>
	    sforce.connection.sessionId='{!GETSESSIONID()}';
    </script>

    <div class="bootstrap-sf1" ng-app="entryApp" ng-controller="entryController" data-date-format="{!dateLocale}" data-input-data="{!inputData}" data-record-id="{!recordId}" data-object="{!objectPrefix}" data-stage="{!stage}" data-recordtype="{!recordType}" data-sf-relatedObject="{!$RemoteAction.dc3ControllerEntry.getRelatedTypeAhead}" data-sf-doSearch="{!$RemoteAction.dc3ControllerEntry.doSearch}" data-sf-doSave="{!$RemoteAction.dc3ControllerEntry.doSave}" data-sf-isRecordType="{!$RemoteAction.dc3ControllerEntry.isRecordType}" data-sf-getLayout="{!$RemoteAction.dc3ControllerEntry.getLayout}">
        <div class="container-fluid" >
            <!-- NAV TOP BAR -->
            <div class="row">
                <div class="col-xs-12">

                    <div class="pull-right tools">
                        <a href="#" onClick="dcNavigate('http://ity.vc/app-entry', '')" class="help">Help <i class="s1utility s1utility-help "></i></a>
                    </div>

                </div>
            </div>
            <div class="row" ng-if="entryStage == 'entry' && {!license.features.pageEntry}" ng-controller="insertController">
                <div class="col-xs-12">
                    <div class="row" ng-if="resultCount > 0">
                        <div class="col-xs-12">
                            <div class="panel ">
                                <header class="panel-heading">
                                    {{resultCount}} duplicates found
                                </header>
                                <div class="panel-body">
        
                                    <section ng-repeat="(prefix, resultList) in searchResult" >
                                        <div ng-if="searchResult[prefix].length > 0">
                                            <div class="page-header page-header-compact row">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <h1>
                                                            <i class="s1icon pull-left mg-r-10 {{objectMeta[prefix].Icon}}"></i><span ng-bind-html="objectMeta[prefix].LabelPlural"></span><br /> <span class="page-header-label count"><span ng-bind-html="searchResult[prefix].length" /> results</span>
                                                        </h1>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="view table table-striped table-hover table-condensed">
                                                    <thead>
                                                        <tr>
                                                            <th width="40" class="score">Score</th>
                                                            <th ng-repeat="field in resultField[prefix]"><span ng-bind-html="field.fieldLabel"/></th>
                                                            <th>&nbsp;</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr ng-repeat="result in resultList">
                                                            <td><span ng-bind-html="result.score"/>%</td>
                                                            <td ng-repeat="field in resultField[prefix]"><span ng-bind-html="objectMap[result.objectData.Id][field.fieldName]"/></td>
                                                            <td class="text-right">
                                                                <a class="btn btn-xs open btn-warning" ng-click="openObject(result.objectData.Id)">Open</a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            
                                            
                                        </div>
                                    </section>
        
        
                                </div>
                                
                            </div>
        
                        </div>
                    </div> 
                </div> 
                <div class="col-xs-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="row">
                                <h2 class="panel-title col-xs-4 text-left">{!IF(isUpdate, 'Update', 'Insert')}&nbsp;{!$ObjectType[objectName].label}</h2>
                                <div class="col-xs-4 text-center">
                                    <button class="btn btn-success btn-sm" ng-disabled="isSaving || entry.$error.required" ng-click="doSave()">
                                        <span ng-show="!isSaving && entry.$error.required">Provide Required Fields</span>
                                        <span ng-show="!isSaving && !entry.$error.required">Save</span>
                                        <span ng-show="isSaving"><i class="s1utility s1utility-spinner s1utility-animate"></i> One moment please</span>
                                    </button>
                                </div>
                                <div class="col-xs-4 text-right"><h6 ng-show="isSearching == true"><i class="s1utility s1utility-spinner s1utility-animate"></i> Searching...</h6></div>
                            </div>
                        </div>
                        <div class="panel-body">
                            
                            <h6 ng-if="!layout.sectionList"><i class="s1utility s1utility-spinner s1utility-animate"></i>fetching layout..</h6>
                            
                            
                            <form class="form-horizontal" name="entry">
                            <section ng-repeat="section in layout.sectionList">
                                <div class="page-header">
                                    <h3 ng-bind-html="section.sectionHeader"></h3>
                                </div>
                                <div class="row" ng-repeat="row in section.sectionRows" >
                                    <div class="col-xs-12 col-sm-{{section.columns == 1 ? 12 : 6}}" ng-repeat="content in row.fieldList">
                                        <div class="form-group hidden-xs" ng-if="content.isPlaceholder">&nbsp;</div>
                                        
                                        <div class="form-group" ng-if="!content.isPlaceholder"> 
                                            
                                            <label class="col-xs-12 col-sm-{{section.columns == 1 ? 2 : 4}} text-xs-left text-sm-right"><span ng-bind-html="content.label"/><span ng-if="content.isRequired" class="warning">&nbsp;*</span></label>
                                            <div class="col-xs-12 col-sm-{{section.columns == 1 ? 10 : 8}}">
                                                <div class="row">
                                                <div class="col-xs-12 col-sm-{{12 / content.fieldList.length}}" ng-repeat="field in content.fieldList" ng-switch="field.field">
                                                
                                                    <div ng-switch-when="OwnerId">
                                                        <input name="{{field.field}}"  ng-if="field.displayType == 'REFERENCE'" class="form-control input-sm" typeahead-loading="loading[field.field]" typeahead-wait-ms="500" typeahead-min-length="3" ng-model="ownerName" typeahead-on-select="input[field.field] = $item.id" typeahead="related.name for related in getLookup($viewValue, (field.referenceObjects | join))" type="text" ng-disabled="!content.isEditable" tabindex="field.tabOrder"/> <i ng-show="loading[field.field]" class="glyphicon glyphicon-refresh" ></i>
                                                    </div>
                                                    <div ng-switch-when="RecordTypeId">
                                                        <input name="{{field.field}}"  ng-if="field.displayType == 'REFERENCE'" class="form-control input-sm" typeahead-loading="loading[field.field]" typeahead-wait-ms="500" typeahead-min-length="3" ng-model="recordTypeName" typeahead-on-select="input[field.field] = $item.id" typeahead="related.name for related in getLookup($viewValue, (field.referenceObjects | join))" type="text" ng-disabled="!content.isEditable" tabindex="field.tabOrder"/> <i ng-show="loading[field.field]" class="glyphicon glyphicon-refresh" ></i>
                                                    </div>
                                                    <div ng-switch-default="ng-switch-default">
                                                    	<input name="{{field.field}}" ng-if="field.displayType == 'STRING'" ng-blur="doSearch(field.field)" ng-required="content.isRequired" ng-maxlength="field.fieldLength" ng-class="{'has-warning':content.isRequired}" type="text" class="form-control input-sm" ng-model="input[field.field]" ng-disabled="!content.isEditable" tabindex="field.tabOrder" /> 
                                                        <input name="{{field.field}}"  ng-if="field.displayType == 'BOOLEAN'" ng-blur="doSearch(field.field)" ng-required="content.isRequired" type="checkbox" ng-model="input[field.field]"  ng-disabled="!content.isEditable" tabindex="field.tabOrder"/> 
                                                        <input name="{{field.field}}"  ng-if="field.displayType == 'ID'" ng-blur="doSearch(field.field)" ng-required="content.isRequired" type="text" maxlength="18" class="form-control input-sm" ng-model="input[field.field]" ng-disabled="!content.isEditable" tabindex="field.tabOrder"/> 
                                                        <input name="{{field.field}}"  ng-if="field.displayType == 'EMAIL'" ng-blur="doSearch(field.field)" ng-required="content.isRequired" ng-maxlength="field.fieldLength" type="EMAIL" class="form-control input-sm" ng-model="input[field.field]"  ng-disabled="!content.isEditable" tabindex="field.tabOrder"/> 
                                                        <textarea name="{{field.field}}"  ng-if="field.displayType == 'TEXTAREA'" rows="3" ng-blur="doSearch(field.field)" ng-required="content.isRequired" ng-maxlength="field.fieldLength" class="form-control" ng-model="input[field.field]" ng-disabled="!content.isEditable" tabindex="field.tabOrder"></textarea> 
                                                        <input name="{{field.field}}"  ng-if="field.displayType == 'CURRENCY'" ng-blur="doSearch(field.field)" ng-required="content.isRequired" type="text" step="any" class="form-control input-sm" ng-model="input[field.field]"  ng-disabled="!content.isEditable" tabindex="field.tabOrder"/> 
                                                        <input name="{{field.field}}"  ng-if="field.displayType == 'DOUBLE'" ng-blur="doSearch(field.field)" ng-required="content.isRequired" type="text" step="any" class="form-control input-sm" ng-model="input[field.field]"  ng-disabled="!content.isEditable" tabindex="field.tabOrder"/> 
                                                        <input name="{{field.field}}"  ng-if="field.displayType == 'INTEGER'" ng-blur="doSearch(field.field)" ng-required="content.isRequired" type="text" step="any" class="form-control input-sm" ng-model="input[field.field]"  ng-disabled="!content.isEditable" tabindex="field.tabOrder"/> 
                                                        <input name="{{field.field}}"  ng-if="field.displayType == 'PERCENT'" ng-blur="doSearch(field.field)" ng-required="content.isRequired" type="text" step="any" class="form-control input-sm" ng-model="input[field.field]"  ng-disabled="!content.isEditable" tabindex="field.tabOrder"/> 
                                                        <input name="{{field.field}}"  ng-if="field.displayType == 'PHONE'" ng-blur="doSearch(field.field)" ng-required="content.isRequired" ng-maxlength="field.fieldLength" type="tel" class="form-control input-sm" ng-model="input[field.field]"  ng-disabled="!content.isEditable" tabindex="field.tabOrder"/> 
                                                        <input name="{{field.field}}"  ng-if="field.displayType == 'URL'" ng-blur="doSearch(field.field)" ng-required="content.isRequired" ng-maxlength="field.fieldLength" type="url" class="form-control input-sm" ng-model="input[field.field]"  ng-disabled="!content.isEditable" tabindex="field.tabOrder"/> 
                                                        <textarea name="{{field.field}}"  ng-if="field.displayType == 'BASE64'" ng-blur="doSearch(field.field)" ng-required="content.isRequired" ng-maxlength="field.fieldLength" class="form-control input-sm" ng-model="input[field.field]" ng-disabled="!content.isEditable" tabindex="field.tabOrder"></textarea> 
                                                        <input name="{{field.field}}"  ng-if="field.displayType == 'TIME'" ng-blur="doSearch(field.field)" ng-required="content.isRequired" type="time" class="form-control input-sm" ng-model="input[field.field]"  ng-disabled="!content.isEditable" tabindex="field.tabOrder"/> 
                                                        <input name="{{field.field}}"  ng-if="field.displayType == 'ENCRYPTEDSTRING'" ng-blur="doSearch(field.field)" ng-required="content.isRequired" ng-maxlength="field.fieldLength" type="text" class="form-control input-sm" ng-model="input[field.field]"  ng-disabled="!content.isEditable" tabindex="field.tabOrder"/> 
                                                        
                                                        
                                                        <div ng-if="field.displayType == 'DATE'">
                                                        	<p class="input-group">
                                                        		<input name="{{field.field}}" datepicker-popup="{!dateLocale}" starting-day="1" is-open="loading[field.field]" ng-blur="doSearch(field.field)" ng-required="content.isRequired" type="text" class="form-control input-sm" ng-model="input[field.field]"  ng-disabled="!content.isEditable" tabindex="{{field.tabOrder}}"/> 
                                                        		<span class="input-group-btn">
                													<button type="button" class="btn btn-default btn-sm input-sm" ng-click="loading[field.field] = true" ng-disabled="!content.isEditable"><i class="glyphicon glyphicon-calendar"></i></button>
              													</span>
                                                        	</p>
                                                        </div>
                                                        
                                                       <div ng-if="field.displayType == 'DATETIME'"> 
                                                        <a class="dropdown-toggle" id="{{field.field}}-drop" role="button" data-toggle="dropdown" data-target="#" href="#">
													        <div class="input-group">
													        	<span class="form-control input-sm" ng-bind-html="input[field.field] | date:'{!dateTimeLocale}'"/>
													        	<span class="input-group-btn">
													        		<button type="button" class="btn btn-default btn-sm input-sm"><i class="glyphicon glyphicon-calendar"></i></button>
													        	</span>
													        	
													        </div>
													      </a>
													      <ul class="dropdown-menu mg-l-15" role="menu" aria-labelledby="dLabel">
													        <datetimepicker  data-on-set-time="doSearch('{{field.field}}')" ng-required="content.isRequired" ng-disabled="!content.isEditable" data-ng-model="input[field.field]" data-datetimepicker-config="{ dropdownSelector: '#{{field.field}}-drop', startView:'year', minView:'minute', minuteStep:1 }"/>
													      </ul>
                                                        </div>
                                                       
                                                        <div ng-if="field.displayType == 'MULTIPICKLIST' && !field.DependentPicklist">
                                                        	<select name="{{field.field}}" id="{{field.field}}" ng-change="doPickListSelect(field.field)" ng-required="content.isRequired" multiple="multiple" class="form-control input-sm" ng-model="input[field.field]" selectpicker="selectpicker" data-width="100%" selectpicker-option="field.pickList" ng-options="pick.value as pick.label for pick in field.pickList" ng-disabled="!content.isEditable" tabindex="field.tabOrder"></select> 
                                                        </div>
                                                        <div ng-if="field.displayType == 'MULTIPICKLIST' && field.DependentPicklist">
                                                        	<select name="{{field.field}}" id="{{field.field}}" ng-change="doPickListSelect(field.field)" ng-required="content.isRequired" multiple="multiple" class="form-control input-sm" ng-model="input[field.field]" selectpicker="selectpicker" data-width="100%" selectpicker-option="dependentPick[field.field]" ng-options="pick.value as pick.label for pick in dependentPick[field.field]" ng-disabled="!content.isEditable || !dependentPick[field.field]" tabindex="field.tabOrder"></select> 
                                                        </div>
                                                        <select name="{{field.field}}"  ng-if="field.displayType == 'PICKLIST' && field.DependentPicklist" ng-change="doPickListSelect(field.field)" ng-required="content.isRequired" class="form-control input-sm" ng-model="input[field.field]" ng-options="pick.value as pick.label for pick in dependentPick[field.field] " ng-disabled="!content.isEditable || !dependentPick[field.field]" tabindex="field.tabOrder">
                                                        </select>
                                                        <select name="{{field.field}}"  ng-if="field.displayType == 'PICKLIST' && !field.DependentPicklist" ng-change="doPickListSelect(field.field)" ng-required="content.isRequired" class="form-control input-sm" ng-model="input[field.field]" ng-options="pick.value as pick.label for pick in field.pickList " ng-disabled="!content.isEditable" tabindex="field.tabOrder">
                                                        
                                                        </select> 
                                                        
                                                        <div ng-if="field.displayType == 'REFERENCE'">
                                                        	<input name="{{field.field}}" placeholder="Please start typing to search" ng-blur="doSearch(field.field)" ng-required="content.isRequired" class="form-control input-sm" typeahead-loading="loading[field.field]" typeahead-wait-ms="500" typeahead-min-length="3" ng-model="input[fieldMeta[field.field].RelatedField]" typeahead-select-on-blur="true" typeahead-select-on-exact="true" typeahead-on-select="input[field.field] = $item.id" typeahead="related.name for related in getLookup($viewValue, (field.referenceObjects | join))" type="text" ng-disabled="!content.isEditable" tabindex="field.tabOrder"/> 
                                                        	<i ng-show="loading[field.field]" class="glyphicon glyphicon-refresh">&nbsp;Searching..</i>
                                                        </div>
                                                    
                                                    </div>
                                                </div>
                                            
                                                
                                                
                                                
                                                
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                        
                                </div>
                            </section>
                            </form>
                            
                            
                        </div>
                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-xs-12 text-center text-xs-left">
                                    <button class="btn btn-success btn-sm" ng-disabled="isSaving || entry.$error.required" ng-click="doSave()">
                                        <span ng-show="!isSaving && entry.$error.required">Provide Required Fields</span>
                                        <span ng-show="!isSaving && !entry.$error.required">Save</span>
                                        <span ng-show="isSaving"><i class="s1utility s1utility-spinner s1utility-animate"></i> One moment please</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" ng-if="entryStage == 'object' && {!license.features.pageEntry}">
                <div class="col-xs-12">
                    <div class="panel">
                        <header class="panel-heading">
                            Select Object
                        </header>
                        <div class="panel-body">
                            <apex:repeat value="{!objectMeta}" var="meta">
                                <div class="col-xs-12 col-sm-3">
                                    <a href="#" ng-click="selectObject('{!meta.Prefix}')">
                                    <div class="well well-sm ">
                                        <i class="s1icon {!meta.Icon} "></i>
                                        {!meta.Label}
                                       
                                    </div>
                                    </a>
                                </div>
                            </apex:repeat>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
            
            <div class="row" ng-if="entryStage == 'recordType' && {!license.features.pageEntry}">
                <div class="col-xs-12">
                    <div class="panel">
                        <header class="panel-heading">
                            Select Record Type
                        </header>
                        <div class="panel-body">
                            
                                <div class="col-xs-12 col-sm-3" ng-repeat="rt in recordTypeList">
                                    <a href="#" ng-click="selectRecordType(rt.recordTypeId)">
                                    <div class="well well-sm " >
                                        <div class="row">
                                        <div class="col-xs-10 pull-left" ng-bind-html="rt.Name"/>
                                        <div class="col-xs-2 pull-right text-right" ng-if="rt.isDefault"><i class="s1utility s1utility-favorite"></i></div>
                                        </div>
                                    </div>
                                    </a>
                                </div>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
            
            <div class="row" ng-if="{!NOT(license.features.pageEntry)}" >
                <div class="col-xs-12">
                    <div class="panel">
                        <header class="panel-heading">
                            Not licensed
                        </header>
                        <div class="panel-body">
                            You are not licensed to use Entry feature of Duplicate Check for Salesforce1. Please contact your Salesforce administrator.
                            
                        </div>
                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-xs-12 text-right">
                                    <button class="btn btn-success" onclick="dcNavigate('/apex/dc3License','')">Show License</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div> 
    
</apex:page>