trigger MultiOppTriger on Opportunity (after insert,after update) 
{
if(Trigger.isAfter)
{
    if(Trigger.isInsert)
    {
        Set<Id> accId=new Set<Id>();
        List<Opportunity> opp1=new List<Opportunity>(); 
        List<Account> acc = new List<Account>();
        Decimal SumofAmount;
        for(Opportunity opp:Trigger.new)
        {
           accId.add(opp.AccountId);
        }
       
        acc = [SELECT Id,Trigger_Status__c,(Select Id,Amount from Opportunities ) FROM Account WHERE Id IN: accId];
        for( Account a : acc){
            List<Opportunity> oppList=a.getSObjects('Opportunities');
            a.Amount__c=0;
            for(Opportunity o: oppList){
                a.Amount__c  += o.Amount;   
                
            }
            
            if(a.Amount__c<=100000)a.Trigger_Status__c ='Low';
            if(a.Amount__c>100000 && a.Amount__c<=500000)a.Trigger_Status__c ='Medium';
            if(a.Amount__c>500000)a.Trigger_Status__c ='High';
       }
update acc;
    }}
    
}