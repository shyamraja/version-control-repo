/********************************************************************
Description:Count Number of Contacts Inserted inside the Account 
        whenever inserted or deleted.
*********************************************************************/ 

trigger updateContactCountOnAccount on Contact ( after insert,after update,after delete) 
{ 
if(Trigger.isInsert) {
   HelperTrigger.InsertContact(Trigger.new);
    }

if(Trigger.isdelete) {
   HelperTrigger.DeleteContact(Trigger.old);
   }
}