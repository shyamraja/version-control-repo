/**********************************************************************
Description:Update Is Modified field in Account When any Field is 
updated in Account and also in Contact
 **********************************************************************/


trigger UpdateField on Account (before update,after update) {
    AccountTriggerHelper add=new AccountTriggerHelper();
  

	if(Trigger.isAfter && trigger.isUpdate){
		if(AccountTriggerHelper.runAfter==false){
			add.onAfterInsert(trigger.new,Trigger.oldMap);
		}
	}


}