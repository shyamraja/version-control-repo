public class HelperTrigger {
 
    public static void InsertContact(List<contact> conlist){
        List<Account> AcctToUpdate = new List<Account>();
        Set<Id> AccountIds = new Set<Id>(); 
        for(Contact c:conlist){
            AccountIds.add(c.AccountId);
        }
        AcctToUpdate=[Select Id,NumberContacts__c,(Select Id from Contacts) From Account where Id IN : AccountIds];{
            for (Account acc : AcctToUpdate ) {
                if(acc.getsObjects('Contacts')<>NULL){
                    acc.NumberContacts__c=acc.getsObjects('Contacts').size();
                } 
            } 
        }
    }
    public static void DeleteContact(List<contact> conlist){
        List<Account> AcctToUpdate = new List<Account>();
        Set<Id> AccountIds = new Set<Id>(); 
        for(Contact c:conlist){
            AccountIds.add(c.AccountId);
        }
        AcctToUpdate= [Select Id,NumberContacts__c,(Select Id from Contacts) From Account where Id IN: AccountIds];{
            for(Account acc:AcctToUpdate ) {
                if(acc.getsObjects('Contacts')<>NULL){
                    acc.NumberContacts__c=acc.getsObjects('Contacts').size();
                } 
                else
                {
                    acc.NumberContacts__c=0;
                }
            }
        }
        
        
        update AcctToUpdate ;
    }
}