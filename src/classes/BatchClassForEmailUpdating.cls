/****************************************************************************
Description:Batch Class to update Site and Contacts Email as same as Account
****************************************************************************/

global class BatchClassForEmailUpdating implements Database.Batchable<SObject> {
    // Start Method
        global Database.QueryLocator start(Database.BatchableContext BC) {
        String query='Select ID,Name,Email__c,Site,(Select Id,Email From Contacts) From Account';
        return Database.getQueryLocator(query);
        }
    // Execute Logic
        global void execute(Database.BatchableContext BC, List<Account> scope){
        List<Contact> con = new List<Contact>();
        try
        {
           for(Account ac: scope){
           	  ac.Site = 'Banglore'; 
             for(Contact c:ac.Contacts){
                c.Email=ac.Email__c;
                con.add(c);
             }
            }
             update con;  
             update scope;
         }
         catch(Exception e){
             System.debug('**************Error Occurred**************'+e.getMessage());
        }
       } 
     global void finish(Database.BatchableContext BC){
     }
}