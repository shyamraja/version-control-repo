global class TableFormatCon{

      public String contactId{ get; set; }
      public String searchValue { get; set; }
      public String ConLastName{ get; set;}
      public String PhoneNo{get;set;}
       
  
   @RemoteAction
        global static List<Account> acclist(){
          
           try{
                return [SELECT Id, Name, BillingCity FROM Account ORDER BY name limit 20 ];   
               }catch(Exception ex){
                  system.debug(ex.getMessage());
                  return null;
               }
              
        }
        
   @RemoteAction
        global static List<Contact>conlist(String contactId){
          
           try{
                return [SELECT Id,Name,Email FROM Contact Where AccountId =: contactId];   
               }catch(Exception e){
                   system.debug(e.getMessage());
                   return null;
               }
              
        }
        
   // ADDING CONTACTS
        
      @RemoteAction
        global static List<Contact> saveContacts(String ConLastName, String contactId,String PhoneNo){
          
           try{
           
                   Contact c = new Contact();
                   c.LastName=ConLastName;
                   c.Phone=PhoneNo;
                   c.AccountId=contactId;
                   insert c;
                   return [SELECT id,Name,Phone FROM Contact Where AccountId =: contactId];   
                   
                  
               }catch(Exception e){
                   system.debug(e.getMessage());
                   return null;
               
               }
              
        }

         // RETURNING THE SEARCH RECORDS
      
         @RemoteAction
        global static List<Account> returnAccount(String searchValue){
           
           try{
                return [SELECT Id, Name FROM Account WHERE name Like: searchValue Limit 5];   
               }catch(Exception e){
                   system.debug(e.getMessage());
                   return null;
               
               }
              
        }
            
            
            global static List<Account> accDetails(String contactId){
          
           try{
                return [SELECT Id, Name, BillingCity FROM Account where Id=: contactId];   
               }catch(Exception e){
                  system.debug(e.getMessage());
                  return null;
               
               }
              
        }
        
       
}