/*****************************************************************************************
@=========================================================================================
@Purpose:  
@          
@=========================================================================================
@History                                                            
@----------                                                            
@VERSION    AUTHOR              DATE             DETAIL                                 
@1.0 -     Shyamala V        19/03/2015      INITIAL DEVELOPMENT               
******************************************************************************************/     
    public class Clone {
    public CloningParent__c cpid;
    public Clone(ApexPages.StandardController controller) {
      
      cpid= ( CloningParent__c)controller.getRecord();
      }
   
    public Clone()
    {
    }
    public PageReference cloneRec(){  
        
        CloningParent__c cp=new CloningParent__c();
        List<CloningChild__c> cons = new List<CloningChild__c>();
        cp = [SELECT ID, Name FROM CloningParent__c WHERE Id = :cpid.id];
        CloningParent__c cpCopy = cp.clone(false,true);
         cp.Active__c=false;
         update cp;
         cpCopy.Active__c=true;
         insert cpCopy;
        try{ 
        List<CloningChild__c> cc = [SELECT Id,Name,Lookup__c,Industry__c,Rating__c FROM CloningChild__c WHERE Lookup__c = : cp.Id];
        for(CloningChild__c c : cc)
        {
            CloningChild__c conCopy = c.clone(false,true);
            conCopy.Lookup__c= cpCopy.Id;
            cons.add(conCopy);
        }
        insert cons;
        }
        catch(Exception e){
        system.debug('****************Error*******************'+e.getMessage());
        }
        return new PageReference('/'+cpCopy.Id);
        
    }
    }