/*****************************************************************************************
@=========================================================================================
@Purpose: Helper Class for Update Field Trigger To update true if any field is updated. 
@=========================================================================================
@History                                                            
@----------                                                            
@VERSION    AUTHOR              DATE             DETAIL                                 
@1.0 -      Shyamala        16/07/2015      INITIAL DEVELOPMENT    
******************************************************************************************/ 
public with sharing class AccountTriggerHelper {
	public static boolean runAfter = false;
	
	 /*
      @Author:Shyamala
      @Description: This method will update when any field is changed
    */
    public void onAfterInsert(List<Account> newTrigger,Map<id,Account> oldMapTrigger){
    runAfter=true;
    	list<Account> acct = new list<Account>();

		if(Trigger.isUpdate && Trigger.isBefore){
			for(Account acc:newTrigger)
			{
				Account oldrecord=oldMapTrigger.get(acc.id);
	
	
				if((oldrecord.Name!=acc.Name) ||
						(oldrecord.Connect__c!= acc.Connect__c)||
						(oldrecord.City__c!= acc.City__c)||
						(oldrecord.State__c!= acc.State__c)||
						(oldrecord.Email__c!= acc.Email__c))
				{
					acc.Is_Modified__c  = True;
	
	
	
				}
			}
	    
	    }
    }

}