/*****************************************************************************************
@=========================================================================================
@Purpose:Display Theatre Names when City And Movie Name Is Selected
@          
@=========================================================================================
@History                                                            
@----------                                                            
@VERSION    AUTHOR              DATE             DETAIL                                 
@1.0 -      Shyamala        16/07/2015      INITIAL DEVELOPMENT               
******************************************************************************************/ 
public with sharing class FastTickets {
    
    
    public List<Movies_Now__c> TheatreList {get;set;} //List for junction
   
    public string selectedValue{get;set;}
    public string selectedMovie{get;set;}
    public Movies__c mov {get;set;}
    public Theatre__c th {get;set;}
    public List<SelectOption> picklist{get;set;}
    public List<SelectOption> listofcity{get;set;}
    public Boolean hidetheater{get;set;}
    public List<Moviewrapper> tlist {get;set;}
    public Boolean metro {get;set;}
    public Boolean bool {get;set;}
    public Boolean city {get;set;}
    
   
    public FastTickets(ApexPages.StandardController controller) {   
       hidetheater=false; 
       tlist=new List<Moviewrapper>();
       picklist=new List<SelectOption>();
       listofcity=new List<SelectOption>();
       Schema.DescribeFieldResult fieldResult = Theatre__c.City__c.getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
       List<Movies__c> movielist=[Select Name From Movies__c];
       //List<Theatre__c> citylist=[Select City__c From Theatre__c];
       
       for(Movies__c m :movielist){
           picklist.add(new SelectOption(m.Name,m.Name));
           }
           /*for(Theatre__c t:citylist)
           {
           listofcity.add(new SelectOption(t.City__c,t.City__c));
           }*/
           for( Schema.PicklistEntry f : ple){
               listofcity.add(new SelectOption(f.getLabel(), f.getValue()));
           }
       }
    
       public pagereference fetchTheaters(){
       try{
            TheatreList= new List<Movies_Now__c>();//memory
            TheatreList = [SELECT Theatre__r.Name,Theatre__r.City__c,Movie_Name__r.Name,Release_Date__c,Seat_Layout__c FROM Movies_Now__c WHERE Theatre__r.City__c =:selectedValue AND Movie_Name__r.Name=:selectedMovie];
                if(TheatreList.size()>0){
                    hidetheater=true;
                }
                system.debug('****************************'+TheatreList);
            
                if(TheatreList.size()==0) {
                    hidetheater=false;
                system.debug('************inside if****************');
              
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Movies arent playing'));
                return null;
                }
                 
         }
         catch(Exception e){
               system.debug('----------------Error Message------------------'+e.getMessage());
               ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
               return null;
               } 
        
         tlist = new List<Moviewrapper>();
         for(Movies_Now__c  movi:TheatreList ){
             Moviewrapper mw=new Moviewrapper();
             mw.mov=movi;
             mw.selected=false;
             tlist.add(mw);
         system.debug('********************'+tlist);
         }
         
         return null;
                   
     }
     public list<Moviewrapper> getDataList(){
        return tlist;
        }
    
    public class Moviewrapper{
            public Movies_Now__c mov{get;set;}
            public boolean selected {get;set;}
            public Moviewrapper(){}
        public Moviewrapper(Movies_Now__c  u){
            mov=u;
            selected=false;
        }
      }
}