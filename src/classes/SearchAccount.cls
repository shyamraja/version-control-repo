public class SearchAccount {

        public List<Account> acc { get; set; }
        public string RecordToDelete { get; set; }
        public Boolean hidename{get;set;}
        public string RecordToSave{get;set;}
        public Boolean showBlock{ get; set; }  
        public Id isEditEnabled {get;set;}
        //public id editing { get; set; }
        Account a;
    
    public SearchAccount (ApexPages.StandardController controller)
    {
         a = (Account) controller.getRecord();
         hidename=false; 
         acc = new List<Account>();
    }
   
    public string searchValue
    {
        get
        {
           
            if(searchValue == null)
               searchValue = '';
            return searchValue;
        }
        set;
    }
   
   
    public List<Account> searchResults
    {
        get
        {
            
            if(searchResults == null)
                searchResults = new List<Account>();
            return searchResults;
        }
        set;
    }
       
   
  
    public void searchContacts()
    {
    
        if(searchValue != ''){
           
            System.Debug('Initializing search, keyword is : ' + searchValue);       
            String finalSearchValue = searchValue + '%';
            List<Account> acc= new List<Account>();
            acc= [select Id, Name from Account where Name like :finalSearchValue];
            searchResults = acc;
            
             if(searchResults.size()>0){
                 hidename=true;
             }else{
                 hidename=false;
                 ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'No Result Found'));
             }
         }else {
             hidename=false;
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Enter Account Name'));
          
         }
       
    }
    
   public void saveAccount() {  
     
            Account editing;
            for(Account acco:searchResults){
            if(acco.Id==isEditEnabled){
            editing=Acco;
            }
            }
            update searchResults ;
            String finalSearchValue = '%' + searchValue + '%';
            acc= [select Id, Name from Account where Name like :finalSearchValue];
            searchResults = acc;
            isEditEnabled =null;
            }
            
    
    public pageReference DeleteAccount(){
            showBlock = true;
            Account todel=new Account(id=RecordToDelete);
            try{
            acc = new List<Account>();
            delete todel;

            String finalSearchValue = '%' + searchValue + '%';
            acc = [select Id, Name from Account where Name like :finalSearchValue];
            searchResults = acc;
            }
            catch(system.DMLexception e){
            ApexPages.addMessages(e);
            }
            return null;
    }
}