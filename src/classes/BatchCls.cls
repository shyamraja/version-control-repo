global class BatchCls implements Database.Batchable<SObject>
{
        global final String email;
        

        /*global BatchClass1() // Batch Constructor
        {
        }*/
       // Start Method
        global Database.QueryLocator start(Database.BatchableContext BC)
        {
            String query='Select Name,Age__c,Company__c,Id__c,Mobile_No__c,Salary__c From Staging__c st';
         	return Database.getQueryLocator(query);
        }
      
      // Execute Logic
       global void execute(Database.BatchableContext BC, List<Staging__c>scope)
       {
           List<Customer1__c> Cust=new List<Customer1__c>();  
                
             for(Staging__c s: scope)
             {
               
               Customer1__c c=new Customer1__c();
               c.Name=s.Name;
               c.Age__c=Decimal.Valueof(s.Age__c);
               c.Mobile_No__c=s.Mobile_No__c;
               c.Salary__c=Decimal.valueOf(s.Salary__c);
               c.Company__c=s.Company__c;
               c.Id__c=Decimal.ValueOf(s.Id__c);
     		   Cust.add(c);
            }
           system.debug('Customer List ___________'+Cust);
           insert Cust;
      }
       global void finish(Database.BatchableContext BC)
       {
       }
}
            // Logic to be Executed at finish 
            
           /* Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
           String[] toAddresses = new String[] {'shyamalaraja93@gmail.com'};
           mail.setToAddresses(toAddresses);
           mail.setSubject('Record Clean Up Status: ' );
           mail.setPlainTextBody
           ('This is the body of email');
           Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
               
                           
    }
    }*/