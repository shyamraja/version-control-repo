global class Batchclass implements Database.Batchable<sObject>{
  
   global Database.QueryLocator start(Database.BatchableContext BC) 
   {
      String query = 'Select Id,Text_Field1__c from Account';
       return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<Account> scope)
   {
      for(Account acc: scope)
      {
          acc.Text_Field1__c='Hai';
      }
    
            delete scope;
   }

   global void finish(Database.BatchableContext BC){

   }

}