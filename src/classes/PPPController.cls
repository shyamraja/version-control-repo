public class PPPController 
{
    public PriceBookEntry pbe {get;set;}
    
    public PPPController() 
    {        
        pbe = new PriceBookEntry();                     
        
        //pbe =[Select Id,Product2Id,Pricebook2ID,UnitPrice from PriceBookEntry];
    }
    
    public void fetchPrice() {
        Id prodId = pbe.Product2Id;
        Id priceId = pbe.Pricebook2Id;
        pbe = [Select Id, UnitPrice, Pricebook2Id, Product2Id from PriceBookEntry where Product2Id = :prodId and Pricebook2Id =:priceId];
    }
    
    public void save()
    {
        //try
        //{
            insert pbe;
        /*}
        
        Catch(Exception e){
            
        }*/
    }
}