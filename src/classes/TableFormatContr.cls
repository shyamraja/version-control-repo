global with sharing class TableFormatContr {
public String contactid {get;set;}
  public String ContactLastName{ get; set; }

    public TableFormatContr(){}

    @RemoteAction
    global static List<Account> getAccount () {

        List<Account> accList=[select Id,Name,Phone,Rating,Site from Account];
        return accList;

    }
    @RemoteAction
        global static List<Contact> getcontact(String contactid){
          
           try{
                return [SELECT id,Name,Email FROM Contact Where AccountId =: contactid];   
               }catch(Exception e){
               
                   system.debug(e.getMessage());
                     return null;
               
               }
   
       
               

    }
    
           
               }