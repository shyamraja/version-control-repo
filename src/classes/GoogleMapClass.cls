global class GoogleMapClass{

      public String contactId{ get; set; }
      public String searchValue { get; set; }
      public String ConLastName{ get; set;}
       
       
   @RemoteAction
        global static List<Account> acclist(){
          
           try{
                return [SELECT Id, Name, BillingCity FROM Account ORDER BY name limit 10 ];   
               }catch(Exception ex){
                  system.debug(ex.getMessage());
                  return null;
               }
              
        }
        
   @RemoteAction
        global static List<Contact>conlist(String contactId){
          
           try{
                return [SELECT Id,Name,Email FROM Contact Where AccountId =: contactId];   
               }catch(Exception e){
                   system.debug(e.getMessage());
                   return null;
               }
              
        }
        
   // ADDING CONTACTS
        
      @RemoteAction
        global static List<Contact> saveContacts(String ConLastName, String contactId){
          
           try{
           
                   Contact con = new Contact();
                   con.LastName=ConLastName;
                   con.AccountId=contactId;
                   insert con;
                   return [SELECT id, Name FROM Contact Where AccountId =: contactId];   
                   
                  
               }catch(Exception e){
                   system.debug(e.getMessage());
                   return null;
               
               }
              
        }

         // RETURNING THE SEARCH RECORDS
      
         @RemoteAction
        global static List<Account> returnAccount(String searchValue){
           
           try{
                return [SELECT Id, Name FROM Account WHERE name Like: searchValue Limit 5];   
               }catch(Exception e){
                   system.debug(e.getMessage());
                   return null;
               
               }
              
        }
            
            
            global static List<Account> accDetails(String contactId){
          
           try{
                return [SELECT Id, Name, BillingCity FROM Account where Id=: contactId];   
               }catch(Exception e){
                  system.debug(e.getMessage());
                  return null;
               
               }
              
        }
        
       
}