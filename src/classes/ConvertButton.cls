/*************************************************************************
  Description: Creating Class to Convert Enquiry into Client,Contact,Deal.
*************************************************************************/

public class ConvertButton {
    public Enquiry__c enq { get; set; }
    public Client__c cli {get;set;}
    public Contact__c con {get;set;}
    public Deal__c deal {get;set;}
    public String Currentid {get;set;}   
    public Boolean selected {get;set;}   
    
    
    public Convertbutton()
    {
        selected  =FALSE;
        Currentid = apexpages.currentpage().getparameters().get('Enquiryid');
        enq       = [Select ID,Name,Last_Name__C,Company__c,Mobile__c,Lead_Status__c,Email__c FROM Enquiry__c WHERE ID =:Currentid];
        cli       = new Client__c();
        con       = new Contact__c();
        deal      = new Deal__c();
    }
    
    
    public pageReference ConvertToClient()
    {
        pageReference pg;
        try
        {
            cli.Name           =enq.Name;
            cli.Company_Name__c =enq.Company__c;
            cli.Mobile__c      =enq.Mobile__c;
            insert cli;
            
            con.Name            =enq.Name;
            con.Client_Name__c  =cli.id;
            con.Last_Name__c    =enq.Last_Name__c;
            con.Email__c       =enq.Email__c;
            insert con;
            
            if(selected==TRUE)
            {
                deal.Name           =enq.Name;
                deal.Deal_Client__c =cli.id;
                deal.Last_Name__c   =enq.Last_Name__c;
                deal.Mobile__c      =enq.Mobile__c;
                insert deal;
                
                pg = new pageReference('/'+deal.ID);
            }else{
                pg = new pageReference('/'+cli.ID);
            }
            
            delete enq;
            
            
            pg.setRedirect(true);
        }
        catch(Exception e)
        {}
        return pg;
    }
}