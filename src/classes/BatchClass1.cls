global class BatchClass1 implements Database.Batchable<SObject>
{
        global final String email;
        

        global BatchClass1() // Batch Constructor
        {
        }
       // Start Method
        global Database.QueryLocator start(Database.BatchableContext BC)
        {
            String query='Select Name,Age__c,Company__c,Id__c From Staging__c';
         	return Database.getQueryLocator(query);
        }
      
      // Execute Logic
       global void execute(Database.BatchableContext BC, List<Staging__c>scope)
       {
           List<Customer1__c> Cust=new List<Customer1__c>();  
                
             for(Staging__c s: scope)
             {
               
               Customer1__c c=new Customer1__c();
               c.Name__c=s.Name;
               c.Age__c=Integer.Valueof(s.Age__c);
               //c.Mobile_No__c=Integer.Valueof(s.Mobile_No__c);
               //c.Salary__c=Integer.valueOf(s.Integer__c);
               c.Company__c=s.Company__c;
               c.Id__c=Integer.ValueOf(s.Id__c);
     		   Cust.add(c);
            }
           
           insert Cust;
      }
       global void finish(Database.BatchableContext BC)
       {
       }
}
            // Logic to be Executed at finish 
           /* Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
 
            mail.setToAddresses(new String[] {email});
            mail.setReplyTo('shyamalaraja93@gmail.com');
            mail.setSenderDisplayName('Batch Processing');
            mail.setSubject('Batch Process Completed');
            mail.setPlainTextBody('Batch Process has completed');
             
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                       
                   }
}*/