/*************************************************************************
Description:Creating Buttton And Assigning User using Wrapper Class
*************************************************************************/
global with sharing class SaveButton {

   
    public List<User> ulist{set;get;}
    public List<String> usersList{set;get;}
    public Enquiry__c enq { get; set; }
    public string enqid {get;set;}
    public String userid {get;set;} 
    public List<userwrapper> userlist1 {get;set;}
    public List<User> selectedUsers = new List<User>();
    public boolean selectedtable{get;set;}
    public boolean check {get;set;}
    
    public SaveButton() {
    enqid='';
    usersList     = new List<String>();
    ulist         = new List<User>();
    userlist1     = new List<userwrapper>();
    selectedUsers = new List<User>();
    usersList     = new List<String>();
    enq           = new Enquiry__c();
    
    string pastalCode = ApexPages.currentPage().getParameters().get('postalcode') ;
    enqid= ApexPages.currentPage().getParameters().get('Enquiryid') ;
    //enq = [Select ID,Name,Last_Name__C,Company__c,Mobile__c,Lead_Status__c,Email__c,Postal_Code__c FROM Enquiry__c WHERE ID =:enquiryid];
    ulist = [Select ID,Name,City,PostalCode,Email FROM User WHERE PostalCode=:pastalCode ];//:enq.Postal_Code__c
    

    
    for(User u : ulist) {
          userwrapper uw = new userwrapper(u);
          userlist1.add(uw);
    }
    }
    
    public List<userwrapper> getUser()
    {
    for(User us : [select Id, Name, Email from User])
          userlist1.add(new userwrapper(us));
          return userlist1;
    }
    
    public List<user> getSelected()
    {
    selectedUsers.clear();
    for(userwrapper uwrapper : userlist1)
        if(uwrapper.selected == true)
        selectedUsers.add(uwrapper.usr);
          return selectedUsers;
    }
    
    public List<User > GetSelectedUsers()
    {
    if(selectedUsers .size()>0)
    {
        return selectedUsers ;
    }
    else
        return null;
    }    
    {
    System.debug(userid+'******');
    }
    
    public Boolean checkbox {get;set;}  
       public List<User> getUlist() {
       return ulist;
    } 
    public List<String> getUsersList() {
       return userslist;
    }
   
    public SaveButton(ApexPages.StandardController controller) {
       enq  = new Enquiry__c();//to allocate memory
    
    }
    
    public PageReference saveEnquiry() {
       PageReference pg;
    try{
       insert enq;
    
    pg =new PageReference('/'+enq.Id);
       pg.setRedirect(true);
    }
    catch(Exception e)
    {}
       return pg;
    }
    public Pagereference cancelEnquiry(){
    
       PageReference oldpage = new Pagereference('/a0Q/o');
       oldpage.setRedirect(true);
       return oldpage;
    }
   
        
        
  
    public pageReference Assign(){
    pageReference pg;
    try{
    List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
    List<user> selecteduserfromlist =  getSelected();
    system.debug('*******************************'+selecteduserfromlist );
    
    if(selecteduserfromlist.size()>1){
     check=false;
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Only One User to be Selected'));
        return ApexPages.currentPage();
    }else
          
         {
          
          enq.Id=enqid;
          enq.OwnerId = selecteduserfromlist[0].id;
          update enq;
           pg =new PageReference('/'+enqid);
            pg.setRedirect(true);
            return pg;
         }
         check=true;
         for(user u : selecteduserfromlist ) {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toaddress  = new String[] {u.email};
            mail.setToAddresses(toaddress);
            mail.setSubject('NOTIFICATION');
            String body = 'Dear ' + u.Name + ', ';
            body += 'You are Assigned.';
            mail.setHtmlBody(body);
            mails.add(mail); 
            Messaging.sendEmail(mails);
            
           
          
        }
       
        }
        catch(Exception e){
        system.debug('***************ERROR*****************'+e.getMessage());
        }
     return ApexPages.currentPage();
    }
    
   
    public class userwrapper
    {
    public User usr{get; set;}
    public Boolean selected {get; set;}
    public userwrapper(User u)
    {
        usr= u;
        selected = false;
        System.debug(usr+'*******');
    }
    }
    
    
    }